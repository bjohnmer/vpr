Rails.application.routes.draw do
  
  get "/email/active/:id" , to: 'emails#active', as: :active_email
  get "/email/inactive/:id" , to: 'emails#inactive', as: :inactive_email
  resources :emails

  get "/contacts/:id/unread" , to: 'contacts#unread', as: :unread
  resources :contacts, except: [ 
                                # :new , :create, 
                                :destroy, :edit, :update 
                              ]

  get "/article_cover/:id" , to: 'articles#image', as: :see_article_cover
  get "/article/unpublish/:id" , to: 'articles#unpublish', as: :unpublish_article
  get "/article/publish/:id" , to: 'articles#publish', as: :publish_article
  resources :articles
  
  get "/categories_cover/:id" , to: 'categories#image', as: :see_category_cover
  resources :categories

  devise_for :users, controllers: { registrations: "users/registrations" }
  get "/author_cover/:id" , to: 'authors#avatar', as: :see_author_avatar
  
  resources :authors, except: [ :new , :create, :destroy ]
  
  resources :params
  
  get "/image_image/:id" , to: 'images#image', as: :see_image_image
  resources :images


  get "/give_articles/just/:num" , to: 'give_articles#just', as: :give_articles_just

  get "/give_articles/bycategory/:id" , to: 'give_articles#by_category', as: :give_articles_by_category
  get "/give_articles/bycategory/:id/just/:num" , to: 'give_articles#by_category_just', as: :give_articles_by_category_just

  get "/give_articles/byauthor/:id" , to: 'give_articles#by_author', as: :give_articles_by_author
  get "/give_articles/byauthor/:id/just/:num" , to: 'give_articles#by_author_just', as: :give_articles_by_author_just

  resources :give_articles, only: [:index, :show, :just, :by_category, :by_category_just, :by_author, :by_author_just]


  get "/give_categories/just/:num" , to: 'give_categories#just', as: :give_categories_just

  resources :give_categories, only: [:index, :show, :just]
  
  
  get "/give_authors/just/:num" , to: 'give_authors#just', as: :give_authors_just

  resources :give_authors, only: [:index, :show, :just]
  
  get "/give_images/just/:num" , to: 'give_images#just', as: :give_images_just

  resources :give_images, only: [:index, :show, :just]
  
  

  root 'backend#index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
