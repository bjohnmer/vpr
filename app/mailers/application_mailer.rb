class ApplicationMailer < ActionMailer::Base
  default from: "20puntos.com <info@20puntos.com>"
  layout 'mailer'
end
