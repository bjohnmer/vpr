class ContactMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.contact_mailer.thanks_contact.subject
  #
  def thanks_contact( recipient )
  	@contact = recipient
    mail to: recipient.email, subject: "Gracias por contactarnos"
  end

  def contact_received( recipient )
  	@contact = recipient
    mail to: "info@20puntos.com", subject: "Tienes un nuevo contacto", :reply_to => @contact.email
  end

end
