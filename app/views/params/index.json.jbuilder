json.array!(@params) do |param|
  json.extract! param, :id, :key, :value, :description
  json.url param_url(param, format: :json)
end
