json.array!(@images) do |image|
  json.extract! image, :id, :title, :description, :image, :created_at, :updated_at
  json.url image_url(image, format: :json)
end
