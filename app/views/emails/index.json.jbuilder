json.array!(@emails) do |email|
  json.extract! email, :id, :email, :state
  json.url email_url(email, format: :json)
end
