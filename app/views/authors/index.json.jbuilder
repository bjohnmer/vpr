json.array!(@authors) do |author|
  json.extract! author, :id, :fullname, :twitter, :facebook, :website, :resume, :user_id, :avatar, :created_at, :updated_at
  json.url author_url(author, format: :json)
end
