json.array!(@categories) do |category|
  json.extract! category, :id, :name, :description, :color, :icon, :cover, :created_at, :updated_at
  json.url category_url(category, format: :json)
end
