json.array!(@contacts) do |contact|
  json.extract! contact, :id, :email, :fullname, :body, :state
  json.url contact_url(contact, format: :json)
end
