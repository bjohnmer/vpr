class Article < ActiveRecord::Base
	
  # Relationships
  # has_many :articles_categories, :dependent => :destroy
  # has_many :categories, :through => :articles_categories

  has_and_belongs_to_many :categories
  belongs_to :author

  has_attached_file :cover, styles: { medium: "800x800>", thumb: "300x300>" }
  validates_attachment_content_type :cover, :content_type => /\Aimage\/.*\Z/

  validates :title, presence: true
  validates :body, presence: true, length: { minimum: 20 }
  validates :categories, presence: true
  validates :cover, presence: true
  validates :author_id, presence: true

  # Muestra los registros en orden inverso
  scope :rorder, ->{ order("articles.created_at DESC") }
  # Consulta por id del artículo
  scope :by_id, ->(id){ find(id) }
  
  scope :by_category, ->(id){ includes( :categories ).where('categories.id' => id ) }
  scope :by_author, ->(id){ includes( :author ).where('authors.id' => id ) }
  scope :publicados, ->{ where( state: "published") }
  scope :no_publicados, ->{ where( state: "draft") }

  def cover_thumb
    self.cover.url(:thumb)
  end

  def cover_medium
    self.cover.url(:medium)
  end

  def cover_full
    self.cover.url()
  end

  def all_categories
    self.categories.select("id, name, color")
  end

  def owner
    self.author.as_json( :methods => [ :avatar_thumb ])
  end

  include AASM

  aasm column: "state" do
    state :draft, initial: true
    state :published

    event :publish do
      transitions from: :draft, to: :published 
    end

    event :unpublish do
      transitions from: :published, to: :draft 
    end
  end

end
