class Contact < ActiveRecord::Base
	include AASM

	scope :leidos, ->{ where( state: "read") }
	
	scope :no_leidos, ->{ where( state: "unread") }

	scope :ultimos, ->{ order( "created_at DESC") }

	aasm column: "state" do
		state :read
		state :unread, initial: true

		event :read do
			transitions from: :unread, to: :read 
		end

		event :unread do
			transitions from: :read, to: :unread 
		end
	end
end
