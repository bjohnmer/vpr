class Image < ActiveRecord::Base

	has_attached_file :image, styles: { medium: "800x800>", thumb: "500x500>" }
	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

	validates :title, presence: true
    validates :description, presence: true, length: { minimum: 20 }
    validates :image, presence: true
	
	scope :rorder, ->{ order("images.created_at DESC") }
  	
  	def image_thumb
		self.image.url(:thumb)
	end

	def image_medium
		self.image.url(:medium)
	end

	def image_full
		self.image.url()
	end

end
