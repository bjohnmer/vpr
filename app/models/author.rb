class Author < ActiveRecord::Base
  belongs_to :user
  has_many :articles, dependent: :destroy

  has_attached_file :avatar, styles: { medium: "800x800>", thumb: "300x300>" }
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  validates :fullname, length: { minimum: 4 }
  validates :twitter, length: { minimum: 3 }
  validates_format_of :facebook, :with => URI::regexp(%w(http https))
  validates_format_of :website, :with => URI::regexp(%w(http https))
  validates :resume, length: { minimum: 4 }
  validates :user_id, presence: true

  scope :rorder, ->{ order("authors.created_at DESC") }
  scope :by_id, ->(id){ find(id) }
   
  def avatar_thumb
    self.avatar.url( :thumb )
  end
  
  def avatar_medium
    self.avatar.url( :medium )
  end
  
  def avatar_full
    self.avatar.url()
  end

end
