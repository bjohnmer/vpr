class Category < ActiveRecord::Base

	# Relationships
	# has_many :articles_categories, :dependent => :destroy
	# has_many :articles, :through => :articles_categories
	has_and_belongs_to_many :articles

	has_attached_file :cover, styles: { medium: "800x800>", thumb: "300x300>" }
	validates_attachment_content_type :cover, :content_type => /\Aimage\/.*\Z/

    validates :name, presence: true
    validates :description, presence: true, length: { minimum: 20 }
    validates :color, presence: true
    validates :cover, presence: true
    validates :icon, presence: true, length: { maximum: 1 }

    scope :rorder, ->{ order("categories.created_at DESC") }
  	
  	def cover_thumb
		self.cover.url(:thumb)
	end

	def cover_medium
		self.cover.url(:medium)
	end

	def cover_full
		self.cover.url()
	end

end
