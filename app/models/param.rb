class Param < ActiveRecord::Base
	validates :key, length: { minimum: 2 }
	validates :value, presence: true
	validates :description, length: { minimum: 5 }
end
