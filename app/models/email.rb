class Email < ActiveRecord::Base
	validates :email, length: { minimum: 5 }

	scope :activos, ->{ where( state: "active") }
  	scope :no_activos, ->{ where( state: "inactive") }

	include AASM

	aasm column: "state" do
		state :active, initial: true
		state :inactive

		event :inactive do
			transitions from: :active, to: :inactive 
		end

		event :active do
			transitions from: :inactive, to: :active 
		end
	end
end
