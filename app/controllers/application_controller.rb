class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

  protect_from_forgery with: :exception

  before_action :authenticate_user!
  before_action :get_new_contacts
  
  def get_new_contacts
  	
  	@newcontacts = Contact.no_leidos.ultimos.count
  end

  private
	def record_not_found(error)
		redirect_to root_url, notice: error.message
	end 
end
