class FrontendController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.

  after_action :add_visit

  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
  
  protect_from_forgery with: :exception

  private
	def record_not_found(error)
	render :json => [ :error => error.message ]
	end

	def add_visit
		@param = Param.where( key: 'visits_count' ).take
		unless @param.nil?
			@param.update( :value => @param.value.to_i + 1 )
		end
	end
end
