class ContactsController < ApplicationController
  layout "backend"
  
  before_action :set_contact, only: [:show, :unread]
  after_action :send_email, only: [:create]

  # GET /contacts
  # GET /contacts.json
  def index
    @contacts = Contact.paginate( page: params[:page] ).all
  end

  # GET /contacts/1
  # GET /contacts/1.json
  def show

  end

  # GET /contacts/new
  def new
    @contact = Contact.new
  end

  # GET /contacts/1/edit
  # def edit
  # end

  # POST /contacts
  # POST /contacts.json
  def create
    @contact = Contact.new(contact_params)

    respond_to do |format|
      if @contact.save
        format.html { redirect_to contacts_url, notice: 'Contact was successfully created.' }
        format.json { render :show, status: :created, location: @contact }
      else
        format.html { render :new }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contacts/1
  # PATCH/PUT /contacts/1.json
  # def update
  #   respond_to do |format|
  #     if @contact.update(contact_params)
  #       format.html { redirect_to @contact, notice: 'Contact was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @contact }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @contact.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # GET /contacts/1/unread
  # GET /contacts/1/unread.json
  def unread
    respond_to do |format|
      if @contact.may_unread?
        @contact.unread!
        format.html { redirect_to contacts_url, notice: 'Contact was successfully marked as unread.' }
        format.json { render :show, status: :ok, location: @contact }
      else
        format.html { render :edit }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  # def destroy
  #   @contact.destroy
  #   respond_to do |format|
  #     format.html { redirect_to contacts_url, notice: 'Contact was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private

    # Send email
    def send_email
      ContactMailer.thanks_contact(@contact).deliver_now
      ContactMailer.contact_received(@contact).deliver_now
    end
    
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.find(params[:id])
      if @contact.may_read?
        @contact.read!
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_params
      params.require(:contact).permit(:email, :fullname, :body, :state)
    end
end
