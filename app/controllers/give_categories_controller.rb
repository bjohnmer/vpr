class GiveCategoriesController < FrontendController
  
  before_action :set_all_categories, only: [ :index, :just ]

  # GET /give_categories.json
  def index
    respond_to do |format|
      format.html { render text: "Error", status: 404 }
      format.json { render :json => [@categories.rorder.as_json( :methods => [:cover_thumb, :cover_medium, :cover_full ] ) , url: request.base_url ] }
    end
  end

  # GET /give_categories/1.json
  def show
    @category = Category.find( params[:id] )
    respond_to do |format|
      format.html { render text: "Error", status: 404 }
      format.json { render :json => [@category.as_json( :methods => [:cover_thumb, :cover_medium, :cover_full ] ) , url: request.base_url ] }
    end
  end

  # GET /give_categories/just/:num.json
  def just
    respond_to do |format|
      format.html { render text: "Error", status: 404 }
      format.json { render :json => [@categories.limit(params[:num]).as_json( :methods => [:cover_thumb, :cover_medium, :cover_full ] ) , url: request.base_url ] }
    end
  end
  
  private
    def set_all_categories
      @categories = Category.all
    end

end
