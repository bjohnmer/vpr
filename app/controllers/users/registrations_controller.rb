class Users::RegistrationsController < Devise::RegistrationsController
  
  layout "backend", only: [ :edit ]
  after_action :create_author, only: :create

  private

  def create_author
    @user.create_author( :fullname => "My Fullname", 
    			   :twitter => "@mytwitter", 
    			   :facebook => "http://www.facebook.com/myname", 
    			   :website => "http://www.mywebsite.com", 
    			   :resume => "This is My resume")
  end
end