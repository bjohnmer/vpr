class GiveImagesController < FrontendController
  
  before_action :set_all_images, only: [ :index, :just ]

  # GET /give_images.json
  def index
    respond_to do |format|
      format.html { render text: "Error", status: 404 }
      format.json { render :json => [@images.rorder.as_json( :methods => [:image_thumb, :image_medium, :image_full ] ) , url: request.base_url ] }
    end
  end

  # GET /give_images/1.json
  def show
    @image = Image.find( params[:id] )
    respond_to do |format|
      format.html { render text: "Error", status: 404 }
      format.json { render :json => [@image.as_json( :methods => [:image_thumb, :image_medium, :image_full ] ) , url: request.base_url ] }
    end
  end

  # GET /give_images/just/:num.json
  def just
    respond_to do |format|
      format.html { render text: "Error", status: 404 }
      format.json { render :json => [@images.limit(params[:num]).as_json( :methods => [:image_thumb, :image_medium, :image_full ] ) , url: request.base_url ] }
    end
  end
  
  private
    def set_all_images
      @images = Image.all
    end

end
