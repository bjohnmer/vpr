class GiveArticlesController < FrontendController
  
  before_action :set_all_articles, only: [:index, :just, :by_category, :by_category_just, :by_author, :by_author_just ]

  # GET /give_articles.json
  def index
    respond_to do |format|
      format.html { render text: "Error", status: 404 }
      format.json { render :json => [@articles.rorder.as_json( :methods => [:cover_thumb, :cover_medium, :cover_full, :all_categories, :owner ] ) , url: request.base_url ] }
    end
  end

  # GET /give_articles/1.json
  def show
    @article = Article.publicados.by_id( params[:id] )
    
    unless @article.nil?
      @article.update( :visits_count => @article.visits_count.to_i + 1 )
    end
    respond_to do |format|
      format.html { render text: "Error", status: 404 }
      format.json { render :json => [@article.as_json(:methods => [:cover_thumb, :cover_medium, :cover_full, :all_categories, :owner ]) , url: request.base_url ] }
    end
  end

  # GET /give_articles/just/:num.json
  def just
    respond_to do |format|
      format.html { render text: "Error", status: 404 }
      format.json { render :json => [@articles.limit(params[:num]).as_json(:methods => [:cover_thumb, :cover_medium, :cover_full, :all_categories, :owner ]) , url: request.base_url ] }
    end
  end
  
  # GET /give_articles/bycategory/:id.json
  def by_category

    respond_to do |format|
      format.html { render text: "Error", status: 404 }
      format.json { render :json => [ @articles.by_category( params[:id] ).rorder.as_json(:methods => [:cover_thumb, :cover_medium, :cover_full, :all_categories, :owner ]) , url: request.base_url ] }
    end
  end

  # GET /give_articles/bycategory/:id/just/:num.json
  def by_category_just
    respond_to do |format|
      format.html { render text: "Error", status: 404 }
      format.json { render :json => [@articles.by_category( params[:id] ).rorder.limit(params[:num]).as_json(:methods => [:cover_thumb, :cover_medium, :cover_full, :all_categories, :owner ]) , url: request.base_url ] }
    end
  end

  # GET /give_articles/byauthor/:id.json
  def by_author

    respond_to do |format|
      format.html { render text: "Error", status: 404 }
      format.json { render :json => [ @articles.by_author( params[:id] ).rorder.as_json(:methods => [:cover_thumb, :cover_medium, :cover_full, :all_categories, :owner ]) , url: request.base_url ] }
    end
  end

  # GET /give_articles/byauthor/:id/just/:num.json
  def by_author_just
    respond_to do |format|
      format.html { render text: "Error", status: 404 }
      format.json { render :json => [@articles.by_author( params[:id] ).rorder.limit(params[:num]).as_json(:methods => [:cover_thumb, :cover_medium, :cover_full, :all_categories, :owner ]) , url: request.base_url ] }
    end
  end

  private
    def set_all_articles
      @articles = Article.publicados.all
    end

end
