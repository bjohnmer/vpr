class GiveAuthorsController < FrontendController
  
  before_action :set_all_authors, only: [ :index, :just ]

  # GET /give_authors.json
  def index
    respond_to do |format|
      format.html { render text: "Error", status: 404 }
      format.json { render :json => [@authors.rorder.as_json( :methods => [:avatar_thumb, :avatar_medium, :avatar_full ] ) , url: request.base_url ] }
    end
  end

  # GET /give_authors/1.json
  def show
    @author = Author.by_id( params[:id] )
    respond_to do |format|
      format.html { render text: "Error", status: 404 }
      format.json { render :json => [@author.as_json( :methods => [:avatar_thumb, :avatar_medium, :avatar_full ] ) , url: request.base_url ] }
    end
  end

  # GET /give_authors/just/:num.json
  def just
    respond_to do |format|
      format.html { render text: "Error", status: 404 }
      format.json { render :json => [@authors.limit(params[:num]).as_json( :methods => [:avatar_thumb, :avatar_medium, :avatar_full ] ) , url: request.base_url ] }
    end
  end
  
  private
    def set_all_authors
      @authors = Author.all
    end

end
