class AuthorsController < ApplicationController
  
  layout 'seeimages', :only => [ :avatar ]
  layout "backend", :only => [ :index, :show, :edit, :create, :update, :destroy ]

  before_action :authenticate_user!
  before_action :set_author, only: [:show, :edit, :update, :avatar]

  # GET /authors
  # GET /authors.json
  def index
    @authors = Author.paginate( page: params[:page] ).all
  end

  # GET /authors/1
  # GET /authors/1.json
  def show
  end

  # GET /authors/1/edit
  def edit
  end

  # PATCH/PUT /authors/1
  # PATCH/PUT /authors/1.json
  def update
    respond_to do |format|
      if @author.update(author_params)
        format.html { redirect_to @author, notice: 'Author was successfully updated.' }
        format.json { render :show, status: :ok, location: @author }
      else
        format.html { render :edit }
        format.json { render json: @author.errors, status: :unprocessable_entity }
      end
    end
  end

  def avatar    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_author
      @author = Author.by_id(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def author_params
      params.require(:author).permit(:fullname, :twitter, :facebook, :website, :resume, :avatar)
    end
end
