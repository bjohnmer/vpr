class ArticlesController < ApplicationController
  
  layout 'seeimages', :only => [ :image ]
  layout "backend", :only => [ :index, :show, :new, :edit, :create, :update, :destroy ]

  before_action :set_article, only: [:show, :edit, :update, :destroy, :image, :unpublish, :publish]
  before_action :set_categories, only: [:edit, :new, :create, :update ]

  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.paginate( page: params[:page] ).all.rorder
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = current_user.author.articles.new(article_params)

    respond_to do |format|
      if @article.save
        format.html { redirect_to @article, notice: 'Article was successfully created.' }
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /articles/unpublish/:id
  def unpublish
    respond_to do |format|
      if @article.may_unpublish?
        @article.unpublish!
        format.html { redirect_to articles_path, notice: 'Article was successfully marked as draft.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # GET /articles/publish/:id
  def publish
    respond_to do |format|
      if @article.may_publish?
        @article.publish!
        format.html { redirect_to articles_path, notice: 'Article was successfully marked as published.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def image
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.by_id(params[:id])
    end

    def set_categories
      @categories = Category.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:title, :body, :cover, category_ids: [])
    end
end
