class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.text :description
      t.string :color, limit: 7, null: false

      t.timestamps null: false
    end
  end
end
