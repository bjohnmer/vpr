class AddAvatarToAuthor < ActiveRecord::Migration
  def change
    add_attachment :authors, :avatar
  end
end
