class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :title, null: false
      t.text :description, null: true

      t.timestamps null: false
    end
  end
end
