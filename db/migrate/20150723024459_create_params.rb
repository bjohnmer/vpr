class CreateParams < ActiveRecord::Migration
  def change
    create_table :params do |t|
      t.string :key, null: false
      t.string :value, null: false
      t.string :description, null: true

      t.timestamps null: false
    end
  end
end
