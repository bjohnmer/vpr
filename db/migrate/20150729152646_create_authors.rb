class CreateAuthors < ActiveRecord::Migration
  def change
    create_table :authors do |t|
      t.string :fullname, null: false, default: ""
      t.string :twitter, null: false, default: ""
      t.string :facebook, null: false, default: ""
      t.string :website, null: false, default: ""
      t.text :resume, null: true
      t.references :user, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end
