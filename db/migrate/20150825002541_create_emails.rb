class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|
      t.string :email, null: false
      t.string :state, default: "inactive"

      t.timestamps null: false
    end
  end
end
